/*
   --------------------------------------------------------------------------------------------------------------------
   Example sketch/program showing how to read data from a PICC to serial.
   --------------------------------------------------------------------------------------------------------------------
   This is a reader1 library example; for further details and other examples see: https://github.com/miguelbalboa/rfid

   Example sketch/program showing how to read data from a PICC (that is: a RFID Tag or Card) using a reader1 based RFID
   Reader on the Arduino SPI interface.

   When the Arduino and the reader1 module are connected (see the pin layout below), load this sketch into Arduino IDE
   then verify/compile and upload it. To see the output: use Tools, Serial Monitor of the IDE (hit Ctrl+Shft+M). When
   you present a PICC (that is: a RFID Tag or Card) at reading distance of the reader1 Reader/PCD, the serial output
   will show the ID/UID, type and any data blocks it can read. Note: you may see "Timeout in communication" messages
   when removing the PICC from reading distance too early.

   If your reader supports it, this sketch/program will read all the PICCs presented (that is: multiple tag reading).
   So if you stack two or more PICCs on top of each other and present them to the reader, it will first output all
   details of the first and then the next PICC. Note that this may take some time as all data blocks are dumped, so
   keep the PICCs at reading distance until complete.

   @license Released into the public domain.

   Typical pin layout used:
   -----------------------------------------------------------------------------------------
               reader1      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN         5          // Configurable, see typical pin layout above
#define SS_PIN1          53 //N        // Configurable, see typical pin layout above
#define SS_PIN2          48 //E
#define SS_PIN3          49 //S
#define SS_PIN4          47 // W

const byte tableId = 7;
MFRC522::MIFARE_Key key;
MFRC522::StatusCode status;
byte readBlockBuffer[18];
byte readBlockBufferSize = sizeof(readBlockBuffer);
byte blockToRead = 2;
MFRC522 readers[] = {MFRC522 (SS_PIN1, RST_PIN), MFRC522(SS_PIN2, RST_PIN), MFRC522(SS_PIN3, RST_PIN), MFRC522(SS_PIN4, RST_PIN)}; //add 4-th
const char directions[] = {'N', 'E', 'S', 'W'};
byte readersCount = 4;
char gameId[36];
byte directionIndex = 0; //N

const byte charsCount = 64;
char responseChars[charsCount];
const char OK_RESPONSE[] = "ROK";

void setup() {
  Serial.begin(9600);		// Initialize serial communications with the PC
  while (!Serial);		// Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
  Serial1.begin(57600); // wifi-module
  SPI.begin();			// Init SPI bus
  for (byte k = 0; k < 6; k++) {
    key.keyByte[k] = 0xFF;  //keyByte is defined in the "MIFARE_Key" 'struct' definition in the .h file of the library
  }
  for (byte i = 0; i < readersCount; i++) {
    Serial.println("Init reader " + i);
    readers[i].PCD_Init();   // Init reader1
    readers[i].PCD_DumpVersionToSerial();  // Show details of PCD - reader1 Card Reader details
    readers[i].PCD_SetAntennaGain(readers[i].RxGain_38dB);
    //readers[i].PCD_WriteRegister(readers[i].CommandReg, readers[i].PCD_NoCmdChange);
    // Prepare the security key for the read and write functions.
    delay(100);
  }

  while(!Serial1);
  Serial.println("Serial initialized");
  boolean moduleIsReady = false;
  while(!(moduleIsReady = waitWifiModule())) {
    delay(100);
  }

  Serial1.println(OK_RESPONSE);
  delay(2000); // wait 
  while(Serial1.available() > 0) { //empty receive buffer for anycase if there is something
    char unwantedChars = Serial1.read();
  }
}

boolean waitWifiModule() {
  while(!(Serial1.available() > 0)) {
    delay(100);
  }

  char firstChar = Serial1.read();
  char isCommandChar = firstChar == 'R'; 
  Serial.println("Received something");
  Serial.println(firstChar);
  if(isCommandChar) {
      recvWithEndMarker();
      Serial.println(responseChars);
    if(strcmp(responseChars, "RDY") == 0) {
      Serial.println("Wi-fi module is ready!");
      return true;
    } else {
      Serial.println("Wi-fi module is not ready something went wrong!");
    }
  } else {
    Serial.println("Received unexpected char!");
  }

  return false;
}

void getFromDirection(byte directionIndex) {
  String readerNum = "Reader ";
  readerNum += directionIndex + 1;
  readerNum += " Waiting ...";
  Serial.println(readerNum);
  //readers[directionIndex].PCD_Init();
  while (!(readers[directionIndex].PICC_IsNewCardPresent() && readers[directionIndex].PICC_ReadCardSerial())) { //
    delay(250);
  }
  status = readers[directionIndex].PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockToRead, &key, &(readers[directionIndex].uid));
  if (status == MFRC522::STATUS_OK) {
    status = readers[directionIndex].MIFARE_Read(blockToRead, readBlockBuffer, &readBlockBufferSize);
    if (status == MFRC522::STATUS_OK) {
      Serial.print("Read Block 2: ");
      Serial1.write(readBlockBuffer[0]);
      Serial1.write(readBlockBuffer[1]);
      Serial1.println("");
      
      Serial.write(readBlockBuffer[0]);
      Serial.write(readBlockBuffer[1]);
      Serial.println("");
      for (int j = 3 ; j < 16 ; j++) //skip one char which is the delimeter _
      {
        Serial1.write(readBlockBuffer[j]);
        Serial.write(readBlockBuffer[j]);
      }

      Serial.write('\n');
      Serial1.println("");
      Serial1.println(directions[directionIndex]);
    }
  }
  // Halt PICC
  readers[directionIndex].PICC_HaltA();
  // Stop encryption on PCD
  readers[directionIndex].PCD_StopCrypto1();
  //readers[directionIndex].PICC_DumpToSerial(&(readers[directionIndex].uid));
  //readers[directionIndex].PCD_WriteRegister(readers[directionIndex].CommandReg, readers[directionIndex].PCD_NoCmdChange | 0x10);
}

void getGameId() {
  Serial1.println("FG");
  Serial1.println(tableId);
  Serial.println("Waiting for Response!");
  while(Serial1.available() <= 0) { // waiting for response
    delay(100);
  }

  recvWithEndMarker();
  if(responseChars[0] == 0) {
    Serial.println("Got no response!");
  } else {
    Serial.print("Response: ");
    Serial.println(responseChars);
    for(byte i = 0; i < 36; i++) {
      gameId[i] = responseChars[i];
    }

    Serial.print("Game ID: ");
    Serial.println(gameId);
  }
}

void recvWithEndMarker() {
    static byte ndx = 0;
    char endMarker = '\n';
    char rc;
    
    while (true) {
      if(Serial1.available() > 0) {
        rc = Serial1.read();
        int n = rc;
        Serial.println(n);
        if(rc != endMarker && n != 10 && n != 13) {
          responseChars[ndx] = rc;
          ndx++;
          if (ndx >= charsCount) {
              ndx = charsCount - 1;
          }
        } else {
            responseChars[ndx] = '\0'; // terminate the string
            ndx = 0;
            break;
        }
      } else {
        delay(100);
      }
    }
}

void emptyChars() {
  for(byte i = 0; i < charsCount; i++) {
     responseChars[i] = 0;
  }
}

void loop() {
  if(gameId[0] == 0) { // we do not have Active Game
    Serial.println("Getting Active Game!");
    getGameId();
  } else {
      for (byte i = 0; i < readersCount; i++) {
        getFromDirection(directionIndex);
        if (++directionIndex == readersCount) { // index is 4
          directionIndex = 0;
        }
    
        delay(800);
      }
  }
}
