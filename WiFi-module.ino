#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <SoftwareSerial.h>

HTTPClient http;
SoftwareSerial arduino(4,0);
// WiFi Credentials
const char WIFI_SSID[] = "Saitama";
const char  WIFI_PASS[] = "";/enter password
char gameId[37];
const boolean debug = true;

const char READY_RESPONSE[] = "RRDY";
const char OK_RESPONSE[] = "ROK";

const char* GOOGLE_HOST = "http://kibikapi.azurewebsites.net";
const char* FINGERPRINT = "3A B0 B1 C2 7F 74 6F D9 0C 34 F0 D6 A9 60 CF 73 A4 22 9D E8";
String GOOGLE_URL = "/";
const String API_URL = "http://192.168.0.103:7071/api/";
const byte charsCount = 64;
char commandChars[charsCount];
int wifiConnect() {
    if(debug) {
      Serial.print("Connecting to ");
      Serial.println(WIFI_SSID);
    }
    
    WiFi.begin(WIFI_SSID, WIFI_PASS);
    
    // WiFi fix: https://github.com/esp8266/Serial/issues/2186
    //WiFi.persistent(false);
    //WiFi.mode(WIFI_OFF);
   // WiFi.mode(WIFI_STA);
    //WiFi.begin(WIFI_SSID, WIFI_PASS);
    
    unsigned long wifiConnectStart = millis();
    
    while (WiFi.status() != WL_CONNECTED) {
        // Check to see if
        if (WiFi.status() == WL_CONNECT_FAILED) {
          if(debug) {
            Serial.println("Failed to connect to WiFi. Please verify credentials: ");            
          }            
          delay(10000);
        }
        
        delay(500);
        if(debug) {
            Serial.println("...");
        }

        // Only try for 5 seconds.
        if (millis() - wifiConnectStart > 45000) {
          if(debug) {
              Serial.println("Failed to connect to WiFi");
          }
            return 0;
        }
    }

    if(debug) {
      Serial.println("");
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
    }

    return 1;
}

int httpsGET(const char* host, const char* fingerprint, String url) {
    WiFiClientSecure client;
    
    Serial.print("Connecting to: ");
    Serial.println(host);
    if (!client.connect(host, 443)) {
        Serial.println("connection failed");
        return -1;
    }
    
    if (client.verify(fingerprint, host)) {
        Serial.println("certificate matches");
    } else {
        Serial.println("certificate doesn't match");
    }

    // Send GET
    client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "User-Agent: ESP8266\r\n" +
               "Connection: close\r\n\r\n");
    
    Serial.println("GET sent");
    while (client.connected()) {
        String line = client.readStringUntil('\n');
        if (line.startsWith("HTTP/1.1")) {
            // Get HTTP return code
            return line.substring(9,12).toInt();
        }
    }
    
    return -1;
}
 
void setup() {
    arduino.begin(57600);
    Serial.begin(9600);
    Serial.setTimeout(2000);
    commandChars[0] = (char)0;
    if(debug) {
        Serial.println("[-] Connecting to WiFi");
    }

    if (wifiConnect()) {
        String url = GOOGLE_URL;// + "?tag=" + TAG_NAME + "&value=" + soilMoistureLevel;
      //int retCode = httpsGET(GOOGLE_HOST, FINGERPRINT, url);
        //if (retCode==HTTP_CODE_OK) {
        //    Serial.println("[-] Reporting succeeded.");
        //} else {
        //     Serial.print("[!] Reporting failed: Code ");
        //    Serial.println(retCode);          
        //}
        
        while(true){
            arduino.println(READY_RESPONSE);
            //Serial.println(READY_RESPONSE);
            delay(750);
            //wait for OK Reseponse
            if(arduino.available() > 0) { // we got something
              recvWithEndMarker();
              Serial.println(commandChars);
              if(strcmp(commandChars, OK_RESPONSE) == 0) {
                //Serial.println(OK_RESPONSE); // notify about receiving
                break;
              }
            }
        }

        while(arduino.available() > 0) {
          arduino.read();
        }
    }
}

boolean getTableActiveGameId(String tableId) {
 
        http.begin(API_URL + "game/table/" + tableId + "?fields=Id");
        int code = http.GET();
        if(code > 0){
          http.getString().substring(1).toCharArray(gameId, 37);
        }else {       
          Serial.print("Error on sending GET: ");
          
          Serial.println(code);
         }

        http.end();
        return code > 0;
}

boolean sendAnnounce(String gameId, String announceType, String pDirection) {
        http.begin(API_URL + "game/announce");
        http.addHeader("Content-Type", "application/json");
        int code = http.POST("[{\"AnnounceType\":\"" + announceType + "\",\"Direction\":\"" + pDirection + "\",\"GameId\":\"" + gameId + "\",\"RegisterDifference\":1}]");
        if(code <= 0){     
          Serial.print("Error on sending GET: ");
          Serial.println(code);
         }

        http.end();
        return code > 0;
}

boolean sendCard(String gameId, String cardType, String pDirection) {
        http.begin(API_URL + "game/card");
        http.addHeader("Content-Type", "application/json");
        int code = http.POST("[{\"CardType\":\"" + cardType + "\",\"Direction\":\"" + pDirection + "\",\"GameId\":\"" + gameId + "\",\"RegisterDifference\":1}]");
        if(code <= 0){     
          Serial.print("Error on sending GET: ");
          Serial.println(code);
         }

        http.end();
        return code > 0;
}

void recvWithEndMarker() {
    static byte ndx = 0;
    char endMarker = '\n';
    char rc;
    
    while (true) {
      if(arduino.available() > 0) {
        rc = arduino.read();
        int n = rc;
        if(rc != endMarker && n != 10 && n != 13) {
          commandChars[ndx] = rc;
          ndx++;
          if (ndx >= charsCount) {
              ndx = charsCount - 1;
          }
        } else {
            commandChars[ndx] = '\0'; // terminate the string
            ndx = 0;
            break;
        }
      } else {
          yield();
      }
    }
}

void emptyChars() {
  for(byte i = 0; i < charsCount; i++) {
     commandChars[i] = 0;
  }
}

void executeFindGameCommand() {
  emptyChars();
  if(debug) {
    Serial.println("TableId: ");
  }

  do {
    recvWithEndMarker();
    yield();
  }while(commandChars[0] == 0);

  String tableId = commandChars;
  if(debug) {
    Serial.println("Provided TableId: " + tableId);
  }

  boolean fetchedOK = getTableActiveGameId(tableId);
  if(fetchedOK) {
    arduino.println(gameId);
    if(debug) {
      Serial.println(gameId);
    }
  }
}

void executeAnnounceCommand() {
  emptyChars();
  if(debug) {
    Serial.println("AnnounceType: ");
  }

  do {
    recvWithEndMarker();
  }while(commandChars[0] == 0);

  String announceType = commandChars;
  if(debug) {
      Serial.println("Provided AnnounceType: " + announceType);
  }

  emptyChars();

  if(debug) {
    Serial.println("Direction: ");
  }

  do {
    recvWithEndMarker();
    yield();
  }while(commandChars[0] == 0);

  String pDirection = commandChars;
  if(debug) {
    Serial.println("Provided Direction: " + pDirection);
  }
  
  boolean sendOK = sendAnnounce(gameId, announceType, pDirection);
  if(sendOK) {
    if(debug) {
      Serial.println(OK_RESPONSE);
    }
    
    arduino.println(OK_RESPONSE);
  }
}

void executeCardCommand() {
  emptyChars();
  if(debug) {
      Serial.println("CardType: ");
  }

  do {
    recvWithEndMarker();
  }while(commandChars[0] == 0);

  String cardType = commandChars;
  if(debug) {
      Serial.println("Provided CardType: " + cardType);
      Serial.println("Direction: ");
  }

  emptyChars();
  do {
    recvWithEndMarker();
    yield();
  }while(commandChars[0] == 0);

  String pDirection = commandChars;
  if(debug) {
      Serial.println("Provided Direction: " + pDirection);
  }

  boolean sendOK = sendCard(gameId, cardType, pDirection);
  if(sendOK) {
    if(debug) {
      Serial.println(OK_RESPONSE);
    }
    
    arduino.println(OK_RESPONSE);
  }
}

void loop() {
  if(arduino.available()> 0) { // received something
    recvWithEndMarker();
    if(strcmp(commandChars,"FG")==0) {//fetch game command
      if(debug) {
          Serial.println("Executing Command Find Game");
      }
      
      executeFindGameCommand();
    } else if (strcmp(commandChars,"GA")==0) { //announce command
      executeAnnounceCommand();
    } else if(strcmp(commandChars,"GC")==0) { // card command
      executeCardCommand();
    }

    emptyChars();
  }

  yield();
}
