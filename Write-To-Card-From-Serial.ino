/*
   --------------------------------------------------------------------------------------------------------------------
   Example sketch/program showing how to read data from a PICC to serial.
   --------------------------------------------------------------------------------------------------------------------
   This is a reader1 library example; for further details and other examples see: https://github.com/miguelbalboa/rfid

   Example sketch/program showing how to read data from a PICC (that is: a RFID Tag or Card) using a reader1 based RFID
   Reader on the Arduino SPI interface.

   When the Arduino and the reader1 module are connected (see the pin layout below), load this sketch into Arduino IDE
   then verify/compile and upload it. To see the output: use Tools, Serial Monitor of the IDE (hit Ctrl+Shft+M). When
   you present a PICC (that is: a RFID Tag or Card) at reading distance of the reader1 Reader/PCD, the serial output
   will show the ID/UID, type and any data blocks it can read. Note: you may see "Timeout in communication" messages
   when removing the PICC from reading distance too early.

   If your reader supports it, this sketch/program will read all the PICCs presented (that is: multiple tag reading).
   So if you stack two or more PICCs on top of each other and present them to the reader, it will first output all
   details of the first and then the next PICC. Note that this may take some time as all data blocks are dumped, so
   keep the PICCs at reading distance until complete.

   @license Released into the public domain.

   Typical pin layout used:
   -----------------------------------------------------------------------------------------
               reader1      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN         5           // Configurable, see typical pin layout above
#define SS_PIN          49          // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

MFRC522::MIFARE_Key key;
const byte charsCount = 64;
char commandChars[charsCount];

byte sector = 0;
byte blockAddr = 2;
char dataBlock[16];
byte dataBlockSize = sizeof(dataBlock);
boolean waitingToWrite = false;
/**
 * Initialize.
 */
void setup() {
    Serial.begin(115200); // Initialize serial communications with the PC
    while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
    SPI.begin();        // Init SPI bus
    delay(500);
    mfrc522.PCD_Init(); // Init MFRC522 card
    mfrc522.PCD_SetAntennaGain(mfrc522.RxGain_38dB);
    mfrc522.PCD_DumpVersionToSerial();
    // Prepare the key (used both as key A and as key B)
    // using FFFFFFFFFFFFh which is the default at chip delivery from the factory
    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }

    Serial.println(F("Scan a MIFARE Classic PICC to demonstrate read and write."));
    Serial.print(F("Using key (for A and B):"));
    dump_byte_array(key.keyByte, MFRC522::MF_KEY_SIZE);
    Serial.println();
    Serial.println(F("BEWARE: Data will be written to the PICC, in sector #1"));
    Serial.println("Provide Sector, Block, Data To Write");
    emptyChars();
}

void recvWithEndMarker() {
    static byte ndx = 0;
    char endMarker = '\n';
    char rc;
    
    while (true) {
      if(Serial.available() > 0) {
        rc = Serial.read();
        if(rc != endMarker) {
          commandChars[ndx] = rc;
          ndx++;
          if (ndx >= charsCount) {
              ndx = charsCount - 1;
          }
        } else {
            commandChars[ndx] = '\0'; // terminate the string
            ndx = 0;
            break;
        }
      } else {
        delay(100);
      }
    }
}

void emptyChars() {
  for(byte i = 0; i < charsCount; i++) {
     commandChars[i] = 0;
  }
}

void executeSetSectorAndBlock() {
  emptyChars();
  do {
    recvWithEndMarker();
  }while(commandChars[0] == 0);

  byte sectorNumberSize = getNumberFromSerialSize();
  Serial.println("Got Sector Chars Count: ");
  Serial.println(sectorNumberSize);
  char sectorChars[2];
  writeFromSerial(sectorChars, 1);
  Serial.println("Got Command Chars OF: ");
  Serial.println(commandChars);
  sector = stringToByte(sectorChars);
  Serial.print("Provided Sector: ");
  Serial.println(sector);
  emptyChars();

  do {
    recvWithEndMarker();
  }while(commandChars[0] == 0);

  byte blockNumberSize = getNumberFromSerialSize();
  Serial.println("Got Block Chars Count: ");
  Serial.println(blockNumberSize);
  char blockChars[2];
  writeFromSerial(blockChars, 1);
  
  blockAddr = stringToByte(blockChars);
  Serial.print("Provided Block: ");
  Serial.println(blockAddr);

  
  do {
    recvWithEndMarker();
  }while(commandChars[0] == 0);

  for(byte i = 0; i < dataBlockSize; i++) {
    dataBlock[i] = commandChars[i];
  }
  
  Serial.print("Provided DataBlock: ");
  Serial.write(dataBlock);
  waitingToWrite = true;
}

void writeFromSerial(char bufferToWrite[], byte charsToWrite) {
  for(byte nI=0; nI < charsToWrite; nI++) {
    bufferToWrite[nI] = commandChars[nI];
  }
}

byte getNumberFromSerialSize() {
  byte numberSize = 0;
  for(byte n = 0; n < 2; n++) {
    if(commandChars[n] == 0) {
      break;
    }

    numberSize++;
  }

  return numberSize;
}

byte stringToByte(char * src){
    return byte(atoi(src));
}

/**
 * Main loop.
 */
void loop() {
  if(Serial.available()> 0) {// received something
    executeSetSectorAndBlock();
    emptyChars();
  }

  writeToCard();
}

void writeToCard() {
    if ( ! mfrc522.PICC_IsNewCardPresent())
        return;
    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())
        return;

    // Show some details of the PICC (that is: the tag/card)
    Serial.print(F("Card UID:"));
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    Serial.println(mfrc522.PICC_GetTypeName(piccType));

    // Check for compatibility
    if (    piccType != MFRC522::PICC_TYPE_MIFARE_MINI
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
        Serial.println(F("This sample only works with MIFARE Classic cards."));
        return;
    }

    MFRC522::StatusCode status;
    byte buffer[18];
    byte size = sizeof(buffer);
  
      // Authenticate using key A
    Serial.println(F("Authenticating using key A..."));
    status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockAddr, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("PCD_Authenticate() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
    }

    // Write data to the block
    Serial.print(F("Writing data into block ")); Serial.print(blockAddr);
    Serial.println(F(" ..."));
    dump_byte_array(dataBlock, 16); Serial.println();
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockAddr, dataBlock, 16);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Write() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
    }
    Serial.println();

    // Read data from the block (again, should now be what we have written)
    Serial.print(F("Reading data from block ")); Serial.print(blockAddr);
    Serial.println(F(" ..."));
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, buffer, &size);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Read() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
    }
    Serial.print(F("Data in block ")); Serial.print(blockAddr); Serial.println(F(":"));
    dump_byte_array(buffer, 16); Serial.println();

    // Check that data in block is what we have written
    // by counting the number of bytes that are equal
    Serial.println(F("Checking result..."));
    byte count = 0;
    for (byte i = 0; i < 16; i++) {
        // Compare buffer (= what we've read) with dataBlock (= what we've written)
        if (buffer[i] == dataBlock[i])
            count++;
    }
    Serial.print(F("Number of bytes that match = ")); Serial.println(count);
    if (count == 16) {
        Serial.println(F("Success :-)"));
    } else {
        Serial.println(F("Failure, no match :-("));
        Serial.println(F("  perhaps the write didn't work properly..."));
    }
    Serial.println();

    // Dump the sector data
    Serial.println(F("Current data in sector:"));
    mfrc522.PICC_DumpMifareClassicSectorToSerial(&(mfrc522.uid), &key, sector);
    Serial.println();

    // Halt PICC
    mfrc522.PICC_HaltA();
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();
    waitingToWrite = false;
}

/**
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}
